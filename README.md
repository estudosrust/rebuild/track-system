# Trace System
Replica from a specific trace system using RUST

## Goals
- Receive data information, in json format, and store on a database (Postgres)
- Receive a get with a code and perform a product trace
- Show all the sessions stored (get all) or session by id (get one)
- Show all product's session (get all) or a detailed product information (get
  one  + product trace) 

### Steps
- [X] Create the project
- [X] Create domain, repository and api 
    - Business rules on Domain
    - Database and Models on repository
    - API Layer + Request and Response items

- [X] Add serde support
- [X] Add Diesel support
- [ ] Tests
- [ ] Dockerfile
- [ ] Docker compose with postgres and pgadmin
