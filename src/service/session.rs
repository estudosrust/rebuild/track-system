use crate::persistence::session::Session;
use crate::api::audit_request::AuditSessionRequest;
use super::ServiceActions;

pub struct SessionService;

impl SessionService {
   pub fn new() -> SessionService {
      SessionService
   }
}

impl ServiceActions<AuditSessionRequest, Session> for SessionService {
   fn presave(&self, request: AuditSessionRequest) -> AuditSessionRequest {
      info!("Overloading the generic presave method");
      info!("So I can validate the data before save");
      request
   }
}