pub mod session;

use diesel::prelude::*;
use diesel::pg::PgConnection;
use dotenv::dotenv;
use std::env;
use uuid::Uuid;

#[derive(Debug, Clone)]
pub struct DataResult<T> {
   total: i32,
   items: Vec<T>
}

fn connection() -> diesel::PgConnection {

   let database_url = env::var("DATABASE_URL")
   .expect("DATABASE_URL must be set");

   PgConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url))
}

pub trait ConvertRequest<T> {
   type E;

   fn convert_from(data: Self::E) -> T;
}

pub trait Persistence<T> {
   fn get_all() -> DataResult<T>;
   fn get_one(id: String) -> T;
   fn insert(&self) -> T;
}