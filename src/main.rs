mod api;
mod service;
mod schema;
mod persistence;

#[macro_use]
extern crate diesel;

#[macro_use]
extern crate log;
extern crate dotenv;

use dotenv::dotenv;
use crate::api::audit_request::{AuditSessionRequest};
use crate::persistence::*;
use crate::persistence::session::Session;
use crate::service::*;
use crate::service::session::SessionService;

fn main() {
    dotenv().ok();
    println!("Hello, world!");
    std::env::set_var("RUST_LOG", "info");
    env_logger::init();

    let audit_json = r#"
      {
         "user": "xpto",
         "audited": 10,
         "unreadable": 1,
         "unknown": 2,
         "valid": 7,
         "invalid": 0,
         "labels": [
            {
               "sku": "1234512",
               "serial": "1234",
               "barcode": "1234567890123",
               "status": 1 
            }
         ]
      }"#;

      info!("Converting json to AuditSessionRequest");

      let audit_session:AuditSessionRequest = serde_json::from_str(&audit_json).unwrap();

      info!("Calling persistence");
      let r = Session::convert_from(audit_session.clone());

      r.insert();

      info!("Total from getall {:?}", Session::get_all());

      info!("Session by id {:?}", Session::get_one("668f76af-751a-4589-94cd-968ce275caa5".to_string()));
}
