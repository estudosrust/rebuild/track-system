pub mod session;

pub trait ServiceActions<R, P> 
   where P: crate::persistence::Persistence<P> + crate::persistence::ConvertRequest<P, E = R> {
 
   fn convert(&self, request: R) -> P {
      info!("Converting request to data");
      P::convert_from(request)
   }

   fn presave(&self, request: R) -> R {
      info!("Geenric presave");
      request
   }

   fn save(&self, request: R) {
      let req = self.presave(request);
      let data = self.convert(req);

      data.insert();
   }


}