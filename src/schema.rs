table! {
    audit_session (id) {
        id -> Uuid,
        audited -> Int4,
        valid -> Int4,
        invalid -> Int4,
    }
}
