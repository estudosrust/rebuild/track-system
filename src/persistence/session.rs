use super::*;
use uuid::Uuid;
use crate::schema::audit_session;
use crate::schema::audit_session::dsl::*;

#[derive(Debug, Clone, PartialEq, Queryable, Insertable)]
#[table_name="audit_session"]
pub struct Session {
   id: Uuid,
   audited: i32,
   valid: i32,
   invalid: i32
}

impl ConvertRequest<Session> for Session {
   type E = crate::api::audit_request::AuditSessionRequest;

   fn convert_from(data: Self::E) -> Session {
      Session {
         id: Uuid::new_v4(),
         audited: *data.audited(),
         valid: *data.valid(),
         invalid: *data.invalid(),
      }
   }
}

impl Persistence<Session> for Session {
   fn get_all() -> DataResult<Session> { 
      let conn = super::connection();
      let result = audit_session.load::<Session>(&conn).expect("Error loading posts");

      DataResult {
         total: 0,
         items: result
      }
   }

   fn get_one(item: String) -> Session {
      let conn = super::connection();
      let item_id = Uuid::parse_str(&item).expect("Invalid UUID");

      audit_session::table.find(item_id).get_result::<Session>(&conn).expect("Not found")
   }

   fn insert(&self) -> Self {
      info!("Inserting {:?}", self);
      let conn = super::connection();
      
      diesel::insert_into(audit_session::table)
      .values(&*self)
      .get_result(&conn)
        .expect("Error saving new post")
      
   }
}