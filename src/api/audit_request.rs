use serde::Deserialize;
use derive_getters::Getters;

#[derive(Debug, Deserialize, PartialEq, Getters, Clone)]
pub struct AuditLabelRequest {
   sku: String,
   serial: String,
   barcode: String,
   status: u8
}

#[derive(Debug, Deserialize, PartialEq, Getters, Clone)]
pub struct AuditSessionRequest {
   user: String,
   audited: i32,
   unreadable: i32,
   unknown: i32,
   invalid: i32,
   valid: i32,
   labels: Vec<AuditLabelRequest>
}